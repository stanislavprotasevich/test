<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Login;
use App\Http\Requests\Api\Logout;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\Sanctum;

class TokenController
{
    public function request(Login $request)
    {
        $user = User::where('email', $request->username)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'username' => ['The provided credentials are incorrect.'],
            ]);
        }
        if ($user->is_active === 0) {
            return response('This user cannot access the application.', 403);
        }

        return response()->json(['token' => 'Bearer ' . $user->createToken('mobile')->plainTextToken]);
    }

    public function revoke(Logout $request)
    {
        $model = Sanctum::$personalAccessTokenModel;

        if ($accessToken = $model::findToken($request->bearerToken())) {
            $accessToken->delete();
        }

        return response(['result' => 'Logged out']);
    }
}
