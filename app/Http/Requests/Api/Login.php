<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class Login extends FormRequest
{
    public function rules(): array
    {
        return [
            'username' => ['required', 'exists:users,email'],
            'password' => ['required'],
        ];
    }

    public function attributes(): array
    {
        return [
            'username'  => trans('login.labels.username'),
            'password'  => trans('login.labels.password'),
        ];
    }
}
