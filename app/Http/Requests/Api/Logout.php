<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LogOutRequest
 * @package App\Http\Requests\Auth
 */
class Logout extends FormRequest
{
    public function rules(): array
    {
        return [
            'device_token' => ['required'],
        ];
    }

    public function attributes(): array
    {
        return [
            'device_token' => trans('api.device_token'),
        ];
    }
}
