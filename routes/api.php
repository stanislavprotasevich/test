<?php

use App\Http\Controllers\Api\TokenController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', function () {
    return \App\Models\Product::all();
});

Route::get('/products/{product}', function (\App\Models\Product $product) {
    return $product;
});

Route::post('/purchase', function (Request $request) {
    $data = $request->validate([
        'products.*.name'     => ['required', 'min:3'],
        'products.*.img_url'  => ['required', 'url'],
        'products.*.category' => ['required', 'min:5'],
        'products.*.price'    => ['required', 'number'],
    ]);

    return $data;
});

Route::prefix('token')->name('token')->group(function () {
    Route::post('request', [TokenController::class, 'request'])
        ->name('request');

    Route::middleware(['auth:sanctum', 'check_active'])->group(function () {
        // Protected endpoint
        Route::post('revoke', [TokenController::class, 'revoke'])->name('revoke')
            ->middleware('auth:sanctum');
    });
});
